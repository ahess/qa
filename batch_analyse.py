# -*- coding: utf-8 -*-
"""
Created on Thu May 20 14:13:20 2021

@author: aaron
"""
import pandas as pd, numpy as np

import matplotlib.pyplot as plt

import mpld3

def plotQA(din,i_coil=0,to_html='',show_figures=True):
  
  if isinstance(din,list):
    out = analyseList(pd.DataFrame(din),i_coil,show_figures=show_figures)
  elif isinstance(din,pd.core.frame.DataFrame):
    out = analyseList(din,i_coil,show_figures=show_figures)
  elif isinstance(din,pd.core.series.Series):
    out = analysePoint(din)  
  else:
    print('plotQA: Unknown input type: ' + str(type(din)))
    return
    
  if len(to_html)>0:
    text_file = open(to_html, "w")
    text_file.write(out.transpose().to_html(escape=False).replace("\\n","\n"))
    text_file.close()
  
def analyseList(pd_tab,coil_i,show_figures=True):
 
  coils = pd_tab['Coil Name'].unique()
  
  h32 = pd_tab[pd_tab['Coil Name']==coils[coil_i]]
 
  col = h32.columns
  
  # create a pandas table of html
  hist = pd.Series(dtype=str)  # historical plot
  curr = pd.Series(dtype=str)  # current value
  figs = []
  for c in col:
    d0 = h32[c][h32.index[-1]]
    if c == 'Analsys Version':
      continue
    units = h32['Units'][h32.index[-1]][c]
    if type(units)==float and np.isnan(units):
      units = ''
    
    #string just print the last one
    if type(d0) == type('str'):
        print( c +'\t: ' + d0)
        curr[c] = d0
        hist[c] = ''
    elif type(d0) == pd._libs.tslibs.timestamps.Timestamp:
        print('Last QA: ',d0)
        curr[c] = str(d0)
        hist[c] = ''
    
    elif type(d0) == np.ndarray:
      # Images, just show last scans image
      if d0.shape[1] > 2:
        fig,ax1 = plt.subplots(1,1)
        if d0.ndim == 2:
          pos = ax1.imshow(d0)
        elif d0.ndim == 3:
          pos = ax1.imshow(d0[:,:,round(d0.shape[2]/2)],interpolation='nearest')
        ax1.title.set_text(c+' ' + '('+units+')')
        fig.colorbar(pos, ax=ax1,label=units)
        curr[c] = mpld3.fig_to_html(fig)
        hist[c] = ''
        figs.append(fig)
      # must be a graph
      elif d0.shape[1] == 2:
          #print(c)
          #print(d0.shape)
          dat = np.array([d[:,1] for d in h32[c] if type(d)==np.ndarray])
          fig1,ax1 = plt.subplots(1,1)
          fig2,ax2 = plt.subplots(1,1)
          pos = ax1.imshow(dat,aspect='auto',resample=False)
          ax1.title.set_text('Historic '+c)
          fig1.colorbar(pos, ax=ax1,label='('+units+')')
          ax2.plot(d0[:,0],d0[:,1])
          ax2.title.set_text('Current ' + c)
          ax2.set_ylabel('('+units+')')
          ax2.set_xlabel('Date')
          curr[c] = mpld3.fig_to_html(fig2)
          hist[c] = mpld3.fig_to_html(fig1)
          figs.append(fig1)
          figs.append(fig2)
    elif type(d0) == np.float64 and not np.isnan(d0):
          dat = np.array(h32[c].array)
          fig,ax = plt.subplots(1,1)
          ax.plot(h32['DateTime'],dat)
          ax.title.set_text(c)
          ax.set_ylabel('('+units+')')
          ax.set_xlabel('Date')
          #fig.mpld3-image = {image-rendering:pixelated}
          curr[c] = str(d0)+' '+units
          hist[c] = mpld3.fig_to_html(fig)
          figs.append(fig)
          
  if ~show_figures:
    for f in figs: plt.close(f)
    
  return pd.DataFrame([hist,curr],index=['Historic','Current'])
  

def analysePoint(table):
  
  print(table['DateTime'])
  #print(table['Coil Name'])
  col = list(table.axes[0])
  
  
  for c in col:
    d0 = table[c]
    #string just print the last one
    if type(d0) == type('str'):
        print( c +'\t: ' + d0)
    elif type(d0) == pd._libs.tslibs.timestamps.Timestamp:
        print('Last QA: ',d0)
    
    elif type(d0) == np.ndarray:
      # Images, just show last scans image
      if d0.shape[1] > 2:
        fig,ax1 = plt.subplots(1,1)
        if d0.ndim == 2:
          pos = ax1.imshow(d0)
        elif d0.ndim == 3:
          pos = ax1.imshow(d0[:,:,round(d0.shape[2]/2)])
        ax1.title.set_text(c)
        fig.colorbar(pos, ax=ax1)
      # must be a graph
      elif d0.shape[1] == 2:
          #print(c)
          #dat = np.array([d[:,1] for d in h32[c]])
          fig,(ax1) = plt.subplots(1)
          #pos = ax1.imshow(dat,aspect='auto',resample=False)
          #ax1.title.set_text('Historic '+c)
          #fig.colorbar(pos, ax=ax1)
          ax1.plot(d0[:,0],d0[:,1])
          ax1.title.set_text('Current ' + c)
    elif type(d0) == np.float64 and not np.isnan(d0):
      print(c+': '+str(d0))
          #dat = np.array(h32[c].array)
          #fig,ax = plt.subplots(1,1)
          #ax.plot(h32['DateTime'],dat)
          #ax.title.set_text(c)
          
          
"""

def fixList(fname,fname_out):
  file = open(fname,'rb')
  table_list = pickle.load(file)
  file.close()
  
  for t in table_list:
    qa.addUnits(t)
    t['Range'] = qa.getReference(t['Scanner SerialNumber'], t['Coil Name'])
    # if type(t['Noise Correlation']) == np.ndarray:
    #   t['Noise Correlation'] = 10*np.log10(np.abs(t['Noise Correlation']))
  
  file = open(fname_out, 'wb')
  pickle.dump(table_list,file)
  file.close()
  
def writeListToPickleDB(fname):
  file = open(fname,'rb')
  din = pickle.load(file)
  file.close()
  for t in din: qa.saveAndWriteHTML(t)
"""
