# -*- coding: utf-8 -*-
"""
Created on 31 May 2021

@author: aaron

Configuration parameters for QA analyss

these are parameters that a user can edit
"""
from os.path import expanduser
# folder to write html webpage to
html_path = expanduser('~/www/qa/')
# folder to record histroic and current values to
data_path = expanduser('~/www/qa/')


""" 
 =============================================================================
 Get reference values by serial number and coil
  referencecs are in pairs, minimum and maximum
  if no reference given, parameter not tested
 =============================================================================
  """
def getReference(scannerSN,coilName):
  import qa
  #d = p.series[0].dcm[0]
  table = qa.getTable()
  if scannerSN == '18928' and coilName == '32Ch_Head_7T':
      table['Scanner Name'] =  'Investigational_Device_7T'
      table['Coil Name']    =  '32Ch_Head_7T' 
      table['TxReference']  =  [200.0, 250.0]
      table['Frequency']    =  [297.218000, 297.218800] 
      table['EPI tNSR Mean']=  [0.0, 2.0]
      table['EPI tTrend']   =  [-1.0, 1.0]
      table['EPI tStability']   =  [0.0,10.0]  # this is very heigh, 3T is <1
      table['EPI Ghost Level']  =  [0.0, 2.0]
      table['Noise Energy'] =  [1e-11, 3e-11]
      table['Noise Correlation'] = [0.001, 0.1]
      table['Scanner SerialNumber']   =  '18928'
      table['Scanner Software']='N4_VB17A_LATEST_20090307'
  elif scannerSN == '66050' and coilName == 'Head_32':
      table['Scanner Name'] =  'Prisma'
      table['Coil Name']    =  'Head_32' 
      table['TxReference']  =  [220.0, 260.0]
      table['Frequency']    =  [123.254500, 123.256800] 
      table['EPI tNSR Mean']=  [0.0, 1.0]
      table['EPI tTrend']   =  [-0.5, 0.5]
      table['EPI tStability']   =  [0.0,1.0]  
      table['EPI Ghost Level']  =  [0.0, 2.0]
      table['GRE energy']= [4e10, 5e10]
      table['RF Noise peak'] = [0.0, 80.0]
      table['RF Noise FLT peak'] = [0.0, 60.0]
      #table['Noise Energy'] =  [1e-11, 3e-11]
      #table['Noise Correlation'] = [0.001, 0.1]
      table['Scanner SerialNumber']   =  '66050'
      table['Scanner Software']='N4_VE11C_LATEST_20160120'
  elif scannerSN == '66093'  and coilName == 'Head_32':
      table['Scanner Name'] =  'Prisma'
      table['Coil Name']    =  'Head_32' 
      table['TxReference']  =  [240.0, 270.0]
      table['Frequency']    =  [123.258000, 123.260000] 
      table['EPI tNSR Mean']=  [0.0, 1.0]
      table['EPI tTrend']   =  [-0.5, 0.5]
      table['EPI tStability']   =  [0.0,1.0]  
      table['EPI Ghost Level']  =  [0.0, 2.0]
      table['GRE energy']= [3.5e10, 5e10]
      table['RF Noise peak'] = [0.0, 80.0]
      table['RF Noise FLT peak'] = [0.0, 60.0]
      #table['Noise Energy'] =  [1e-11, 3e-11]
      #table['Noise Correlation'] = [0.001, 0.1]
      table['Scanner SerialNumber']   =  '66093'
      table['Scanner Software']='N4_VE11C_LATEST_20160120'
  elif scannerSN == '66050' and coilName == 'HeadNeck_64 ':
      table['Scanner Name'] =  'Prisma'
      table['Coil Name']    =  'HeadNeck_64' 
      table['TxReference']  =  [210.0, 250.0]
      table['Frequency']    =  [123.254500, 123.256800] 
      table['EPI tNSR Mean']=  [0.0, 1.0]
      table['EPI tTrend']   =  [-1, 1]
      table['EPI tStability']   =  [0.0,1.0]  
      table['EPI Ghost Level']  =  [0.0, 2.0]
      table['GRE energy']= [1.8e11, 2.1e11]
      table['RF Noise peak'] = [0.0, 80.0]
      table['RF Noise FLT peak'] = [0.0, 60.0]
      #table['Noise Energy'] =  [1e-11, 3e-11]
      #table['Noise Correlation'] = [0.001, 0.1]
      table['Scanner SerialNumber']   =  '66050'
      table['Scanner Software']='N4_VE11C_LATEST_20160120'
  elif scannerSN == '66093'  and coilName == 'HeadNeck_64 ':
      table['Scanner Name'] =  'Prisma'
      table['Coil Name']    =  'HeadNeck_64' 
      table['TxReference']  =  [230.0, 255.0]
      table['Frequency']    =  [123.258000, 123.260000] 
      table['EPI tNSR Mean']=  [0.0, 1.0]
      table['EPI tTrend']   =  [-1, 1]
      table['EPI tStability']   =  [0.0,1.0]  
      table['EPI Ghost Level']  =  [0.0, 2.0]
      table['GRE energy']= [1.8e11, 2.1e11]
      table['RF Noise peak'] = [0.0, 80.0]
      table['RF Noise FLT peak'] = [0.0, 60.0]
      #table['Noise Energy'] =  [1e-11, 3e-11]
      #table['Noise Correlation'] = [0.001, 0.1]
      table['Scanner SerialNumber']   =  '66093'
      table['Scanner Software']='N4_VE11C_LATEST_20160120'
   
  return table