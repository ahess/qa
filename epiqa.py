
# -*- coding: utf-8 -*-
"""
Created on Tue Mar  2 14:25:28 2021
Replicated functionality from Stuart Clare's FSL based EPI QA tools'
@author: aaron

Usage:
  epi = epiqa.EPIqa(img,TR):
    img is 4D EPI, X by Y by Z by time series
    TR is the TR in seconds
  
  tnsr,  100 * temporal SD of HP Filtered time course divided by mean unflitered:
  mean,mean=epi.tnsr_prc()    # units: %
  
  stability, sqrt of mean temporal variance of HP filtered time course
  tStab = epi.timecourse_stability()  # units: %  
  
  Temporal trend - linear regression
  t = epi.trend()  # units: % change per volume
  
  Intensity of ghosts signal
  ghots = epi.ghost_level() # units: % of image mean
  
Method:
  Data are convereted to percent of percent of mean and masked at 10% of range
  High pass filter is applied, replicating FSL
  
"""
import numpy as np
from scipy import ndimage

class EPIqa(object):
  # img is 4D, dim 0 to 2 are spatial and 3 is time
  def __init__(self, img,TR,threshold=10):
    if img.ndim < 4:
      self.isValid = False
      return
    
    self.isValid = True
    
    # generate_mask 
    # (max signal over time > 10) .. not sure why 10, 50 seems more reasonable, 
    # or a statistic of the data, e.g. mean/10?
    tmax = np.tile(np.amax(img,axis=3)[:,:,:,np.newaxis],[1,1,1,img.shape[3] ])
    up = np.percentile(img,98)
    dw = np.percentile(img,2)
    tmax_pct = 100*(tmax-dw)/up
    im_m =  np.ma.masked_where( tmax_pct < threshold, img)
    
    # percentage_of_mean
    scale = 100.0/float(im_m.mean())
    #for some reason need to remask, so scale applies to background too
    self.img_masked = np.ma.masked_where(tmax_pct < threshold, scale*img)
    
    self.filtered = self.HPFilter(50/TR)

    
    # (frame / mean each masked frame)*100
    
  # global time couse (mean of masked signal)
  def globalTimeCourse(self):
    return self.img_masked.mean(axis=(0,1,2))
  
  # global time course of high pass filtered data
  def globalTimeCourse_dt(self):
    return self.filtered.mean(axis=(0,1,2))
    
  # Replicate fslmaths bptf nonlinear high pass filter
  # matlab example found https://cpb-us-w2.wpmucdn.com/sites.udel.edu/dist/7/4542/files/2016/09/fsl_temporal_filt-15sywxn.m
  def HPFilter(self,HP_sigma):
    HP_filt_size = round(HP_sigma*8)
    HP_lin = np.linspace((-HP_filt_size/2),(HP_filt_size/2),HP_filt_size)
    HP_gfilt = np.exp(-pow(HP_lin,2)/(2*pow(HP_sigma,2)))
    HP_gfilt = np.reshape(HP_gfilt/sum(HP_gfilt),[1,1,1,len(HP_gfilt)])
    
    f = self.img_masked - ndimage.correlate(self.img_masked, HP_gfilt)
    return f
  
  def trend(self):
    y = self.globalTimeCourse()
    N = len(y)
    x = range(N)
    # I think this is a linear regression, can otherwise use sklearn.linear_model import LinearRegression
    t = (sum(i * y[i] for i in x) - 1./N*sum(x)*sum(y)) / (sum(np.power(x,2)) - 1./N*sum(x)**2)

    return round(t*100,2)

  # Method: calculate percent change in detrended data normalised by mean of unfiltered 
  # NOTE: global mean is 100 (normalisation above), this result is therefore a percent
  def timecourse_stability(self):
    stab= np.sqrt(self.filtered.var(axis=3).mean())  
    return round((stab),3)

  # FMRI tSNR is often reported as tNSR reported as percent change of the mean
  def tnsr_prc(self):
    tnsr = 100*self.filtered[:,:,:,2:].std(axis=3)/self.img_masked[:,:,:,2:].mean(axis=3)
    return round(tnsr.mean(),1),tnsr.filled(0)


  def ghost_level(self):
    #Extract a single time frame
    # 1. create an N/2 shifted mask, shifted in x, then in y
    # 2. remove a dilated (x3) mask from each, to only background is used
    # 3. ghost level is mean of ghost mask divided mean of eroded mask (x3)
    
    # extract first time frame, take second image (1) as 7T data has a noise ref first?
    img_m = self.img_masked[:,:,:,1]
    #Find the matrix size
    (x,y,z) = img_m.shape
    
    # NOTE Dilation and erosion are opposite from stuats code
    # because mask indicates what to remove (inverted mask)
    #Dilate the mask (x3)
    mask_d = ndimage.binary_dilation(img_m.mask,iterations=3).astype(self.img_masked.mask.dtype)
    #Erode the mask (x3)
    mask_e = ndimage.binary_erosion(img_m.mask,iterations=3).astype(self.img_masked.mask.dtype)
    
    # circshift in x (get mask for n/2 ghost)
    mask_sx = np.roll(img_m.mask,shift=round(x/2),axis=0)  #m1
    mask_sy = np.roll(img_m.mask,shift=round(y/2),axis=1)
    

    #Subtract the dilated mask from the shifted mask
    mghost1 = np.ma.masked_where(~np.logical_and(~mask_sx,mask_e),img_m.data)
    mghost2 = np.ma.masked_where(~np.logical_and(~mask_sy,mask_e),img_m.data)
    
    #Calculate image mean
    img_m = np.ma.masked_where(mask_d,img_m.data)
    im_mean = img_m.mean()
    
    #Calculate ghost in ghost1 mask
    
    ghost1=(mghost1.mean()*100)/im_mean
    ghost2=(mghost2.mean()*100)/im_mean
    
    #print(ghost1,ghost2)
    #Return the largest value
    return(round(max(ghost1,ghost2),3))

