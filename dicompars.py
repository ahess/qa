# -*- coding: utf-8 -*-
"""
Created on Wed Feb 24 12:29:14 2021

@author: aaron


=============================================================================
Organise DICOM files into patients and series

Load all patients in folder, p is a list of Patients:
p = getPatients(fpath)

show what series in patient 0:
  p0 = p[0]
 print(p[0])
 
extract series index 0:
  s = p0[0]
extract series number 1:
  s = p0['S1']
  
Read in images from series
  im=s.getImages(ori): for one slice orientation return a numpy ndarray of the volume
            dimentions: row, col, slc, echo, coil, inst
  im=s.getImagesAll(): return all image orientations as a list
            dimentions: row, col, slc, echo, coil, inst

Read spectra:
  spec = s.getSpectra()
      dimentions: time, instance, coils
      
Read graphihcs overlay (for RF noise spectra):
  RF1,RF2 = s.getOOG_rf()
  
Get array of TE's (multi echo):
  TEs = s.getTEs()
=============================================================================
"""
import pydicom
import numpy as np
import os
import AsciiMrProt
import struct
from tqdm import tqdm
import warnings
# Read all dicoms in fpath and organise them in to a patient list
def getPatients(fpath, import_sub_dirs=True):
    flist = getFiles(fpath,import_sub_dirs)
    
    warnings.filterwarnings("ignore", category=UserWarning)
    
    dcmlist = [pydicom.dcmread(f,force=True) for f in tqdm(flist)]
    
    # sort into patients
    pateints = []
    
    # remove non dicom files
    dcmlist = [d for d in dcmlist if 'PatientID' in d]
    
    while dcmlist:
        pdcmlist = [d for d in dcmlist if d.PatientID==dcmlist[0].PatientID]
        
        for d in pdcmlist:
            dcmlist.remove(d)
            
        pateints += [Patient(pdcmlist)]
        
    pateints.sort(key= lambda x:x.id)
    return pateints




# list files in directory (and sub directories)
def  getFiles(fpath, import_sub_dirs=False):
    flist = [fpath + os.path.sep + f for f in os.listdir(fpath)]
    
    sub_dirs = [f for f in flist if os.path.isdir(f)]
    
    for f in sub_dirs:
        flist.remove(f )
    
    if import_sub_dirs:
        for d in sub_dirs:
            flist += getFiles(d,import_sub_dirs) 
             
    return flist
 
    
 
"""
=============================================================================
 Class for holding a list of DICOM Series
=============================================================================
"""
class Patient(object):
  def __init__(self, dcmlist):
    self.id = dcmlist[0].PatientID
    #sort into series, then aquisitions
    #self.dcmlist = dcmlist
    self.series = [];
    while dcmlist:
        pdcmlist = [d for d in dcmlist if d.SeriesNumber==dcmlist[0].SeriesNumber]
        
        for d in pdcmlist:
            dcmlist.remove(d)
            
        self.series += [Series(pdcmlist)]
        
    self.series.sort(key= lambda x: x.seriesNumber)
    
  def __str__(self):
      out = self.id + '\n'
      for i,s in enumerate(self.series):
        out+=str(i)+':\tS'+ str(s.seriesNumber)+'\t'+ s.seriesDescription + '\t ' + str(len(s.dcm)) + '\n'
      return out
    
  def __getitem__(self, key):
    if type(key) is int:
      return self.series[key]
    elif type(key) is str:
      si = ''.join(filter(str.isdigit,key)) # extract series number, then look for it
      s = []
      if si:
        s = [s for s in self.series if s.seriesNumber is int(si)]
      if s:
        return s[0]
      else:
        return []
    else:
      return []
        
    
    
"""    
=============================================================================
Class for holding and extracting images in Series
=============================================================================
"""
class Series(object):
  def __init__(self, dcmlist):
    # set properties that apply to series
    self.seriesNumber = int(dcmlist[0].SeriesNumber)
    self.isMosaic = True if [0x0019, 0x100A] in dcmlist[0] else False
    self.isPhase = False
    self.seqenceName = dcmlist[0].SequenceName if 'SequenceName' in dcmlist[0] else ''
    self.seriesDescription = str(dcmlist[0].SeriesDescription) if 'SeriesDescription' in dcmlist[0] else ''
    if 'RepetitionTime' in dcmlist[0]: 
      self.TR = float(dcmlist[0].RepetitionTime) 
    else :
      self.TR = float(-1)
    #test if spectroscopy
    if [0x0029, 0x1008] not in dcmlist[0]:
      self.type = 'UNKNOWN'
    elif dcmlist[0][0x0029, 0x1008].value == 'IMAGE NUM 4':
        self.mrProt = AsciiMrProt.AsciiMrProt(dcmlist[0][0x0029, 0x1020].value.decode(errors='ignore'))
        self.type = 'IMAGE'
    elif dcmlist[0][0x0029, 0x1008].value == 'SPEC NUM 4':
        self.mrProt = AsciiMrProt.AsciiMrProt(dcmlist[0][0x0029, 0x1120].value.decode(errors='ignore'))
        self.type = 'SPECTRA'
    else:
        self.type = 'UNKOWN'

    # sort series sor it can be read in by list order
    # Sort fields: echo, Instance, AcquisitionNumber, silce locaton, 
    #             orientation (slice group), coil ID
    #     furute: phase (time), 
#     if 'EchoTrainLength' in dcmlist[0] and dcmlist[0].EchoTrainLength is not None and int(dcmlist[0].EchoTrainLength)>1: dcmlist.sort(key= lambda x: float(x.EchoTime))
#     dcmlist.sort(key= lambda x: int(x.InstanceNumber))
#     if 'AcquisitionNumber' in dcmlist[0]:     dcmlist.sort(key= lambda x: int(x.AcquisitionNumber))
# #    

#     if 'SliceLocation' in dcmlist[0]:     dcmlist.sort(key= lambda x: float(x.SliceLocation))
#     if [0x0051, 0x100F] in dcmlist[0]:    dcmlist.sort(key= lambda x: x[0x0051, 0x100F].value) # coil ID in pvt header
#    if 'ImageOrientationPatient' in dcmlist[0]: dcmlist.sort(key= lambda x: ''.join([str(i) for i in x.ImageOrientationPatient]))
    
    # break into different orientations (if there are)
    if 'ImageOrientationPatient' in dcmlist[0]:
      ori = [''.join([str(i) for i in d.ImageOrientationPatient] ) for d in dcmlist]
      ori, ori_i = np.unique(ori,return_inverse=True)
      self.n_orientations = len(ori)
    else:
      self.n_orientations = 1;
    
    if self.n_orientations > 1:
      self.ori = [[dcmlist[i] for i in ori_i if i==j] for j in range(self.n_orientations)]
    else:
      self.ori = [dcmlist]
      
    self.dcm = dcmlist
    #potential aquisition idneifiers AcquisitionTime
  def __str__(self):
    out = ''
    out = out + 'Series Desc: \t' + self.seriesDescription + '\n'
    out = out + 'sequenceName: \t' + self.seqenceName + '\n'
    out = out + 'number images: \t'+ str(len(self.dcm)) + '\n'
    out = out + 'type: \t' + str(self.type)+'\n'
    out = out + 'n_orientations: \t' + str(self.n_orientations)+'\n'
    out = out + 'Series: \t' + str(self.seriesNumber) + '\n'
    out = out + 'isMozaic: \t' + str(self.isMosaic)+'\n'
    out = out + 'isPhase: \t' + str(self.isPhase) + '\n'
    out = out + 'TR: \t' + str(self.TR) + '\n'
    
    
    return out
  # return all image orientations in a list
  def  getImagesAll(self):
    if self.n_orientations>1:
      return [self.getImages(i) for i in range(self.n_orientations)]
    else:
      return self.getImages(0)
    
    
  # Read in all images of series into a volume
  def getImages(self,i_ori=0):
    # what size
    if i_ori>=self.n_orientations:
        print('Error: Orientation index exceeds number of orientations, expected < '+str(self.n_orientations))
        return
      
    n_row = int(self.ori[i_ori][0].Rows)
    n_col = int(self.ori[i_ori][0].Columns)
    n_dcm = len(self.ori[i_ori])
    o = np.zeros([n_dcm,1])
    
    # slice, how many?
    if self.getEchoTrainLength()>1: 
      echo =[float(d.EchoTime) for d in self.ori[i_ori]]
      echo, echo_i = np.unique(echo,return_inverse=True)
    else:
      echo_i = o
      echo = [0]
        
    if 'SliceLocation' in self.ori[i_ori][0]:
      slc = [float(d.SliceLocation) for d in self.ori[i_ori]]
      slc, slc_i = np.unique(slc,return_inverse=True)
    else:
      slc_i = o
      slc = [0]
        
      
    if [0x0051, 0x100F] in self.ori[i_ori][0]: 
      coil = [d[0x0051, 0x100F].value for d in self.ori[i_ori]] # coil ID in pvt header
      coil, coil_i = np.unique(coil,return_inverse=True)
    else:
      coil_i = o
      coil = [0]
    
    if 'AcquisitionNumber' in self.ori[i_ori][0]: 
      acq = [int(d.AcquisitionNumber) for d in self.ori[i_ori]] # coil ID in pvt header
      acq, acq_i = np.unique(acq,return_inverse=True)
    else:
      acq_i = o
      acq = [0]
      
    n_echo = len(echo)    
    n_coil = len(coil)
    n_slc  = len(slc)
    n_acq = len(acq) 
    
    #n_inst = int(np.ceil(n_dcm/n_echo/n_slc/n_coil))
    
    if self.isMosaic:
      n_slc = int(self.ori[i_ori][0][0x0019, 0x100A].value)
      nMosaic = np.ceil(np.sqrt(n_slc))
      n_row =int(n_row/nMosaic)
      n_col =int(n_col/nMosaic)
    else:
      n_slc = len(slc)
    
    img = np.ndarray([n_row,n_col,n_slc,n_echo,n_coil,n_acq])
    self.dims = ['row','col','slc','echo','coils','rep']
    #i = 0
    
    for d_i in range(n_dcm):
      if(self.isMosaic):
        img[:,:,:,int(echo_i[d_i]),int(coil_i[d_i]),int(acq_i[d_i])] = deMosaic(self.ori[i_ori][d_i].pixel_array,n_slc)
      else:
        img[:,:,int(slc_i[d_i]),int(echo_i[d_i]),int(coil_i[d_i]),int(acq_i[d_i])] = self.ori[i_ori][d_i].pixel_array
      
      #if d_i<(n_dcm-1)and echo_i[d_i]==echo_i[d_i+1] and slc_i[d_i]==slc_i[d_i+1] and coil_i[d_i] == coil_i[d_i+1]:
      #  i+=1
      #else:
      #  i=0
    
    # record any ordings of interest 
    self.acq_i = acq_i
        
    return img

  def getImagesComplex(self,i_ori=0):
      img = self.getImages(i_ori)
      phs_range = 2**12
      
      return np.exp(1j*2*np.pi*(img/phs_range-0.5))  
  
  # note coil=0 is lost for good in raw Siemens data.
  #    this is the unphased sum of all channels (used for online display)
  #    if the phasing was known we coculd recocver it
  def getSpectra(self):
      if self.type != 'SPECTRA':
          return []
      n_dcm = len(self.dcm)
      o = np.zeros([n_dcm,1])
      if 'ImageComments' in self.dcm[0]: 
        coil = [d['ImageComments'].value for d in self.dcm] # coil ID in pvt header
        coil, coil_i = np.unique(coil,return_inverse=True)
      else:
        coil_i = o
        coil = [0]  
      
      n_coil = len(coil)
      n_inst = int(n_dcm/n_coil)
      
      data = self.dcm[0][0x7fe1,0x1010].value
      data = [complex(real=struct.unpack('f',data[x:x+4])[0],imag=struct.unpack('f',data[x+4:x+8])[0]) for x in range(0, len(data), 8)]
      spec = np.ndarray([len(data),n_inst,n_coil],dtype = complex)
      
      for i in range(n_dcm):
          data = self.dcm[i][0x7fe1,0x1010].value
          data = [complex(real=struct.unpack('<f',data[x:x+4])[0],imag=struct.unpack('f',data[x+4:x+8])[0]) for x in range(0, len(data), 8)]
          if n_inst>1 and 'AcquisitionNumber' in self.dcm[i]:
              inst = int(self.dcm[i].AcquisitionNumber) - 1             
          else:
              inst = 0
          spec[:,inst,int(coil_i[i])] = data
          
      
      return spec
    
    # return the two longest graph plot objects from [0x0029,0x1210]
  def getOOG_rf(self):
      # data is text in a STEP ISO 10303 format, a bit like XML but not
      step = bytes(self.dcm[0][0x0029,0x1210]).decode('ascii').split('\n')
      ind = sorted(range(len(step)), key=lambda k: len(step[k]))
      
      a1 = step[ind[-1]-1].split('(')[3].split(')')[0]  # axis object
      v1 = step[ind[-1]].split('(')[2].split(')')[0]    # vector
      a2 = step[ind[-2]-1].split('(')[3].split(')')[0]
      v2 = step[ind[-2]].split('(')[2].split(')')[0]
      
      
      a1_lim = [float(f) for f in a1.split(',')]
      
      a2_lim = [float(f) for f in a2.split(',')]
      
      vec1 = np.asarray([float(f) for f in v1.split(',')])
      vec1 = vec1.reshape([int(len(vec1)/3),3])
      vec1 = np.delete(vec1,(2), axis=1)
      vec1[:,0] = vec1[:,0]*1000 - 500
      vec1[:,1] = a1_lim[-1]*(1-vec1[:,1])
      vec2 = np.asarray([float(f) for f in v2.split(',')])
      vec2 = vec2.reshape([int(len(vec2)/3),3])
      vec2 = np.delete(vec2,(2), axis=1)
      vec2[:,0] = vec2[:,0]*1000 - 500
      vec2[:,1] = a2_lim[-1]*(1-vec2[:,1])
      
      # return them in the order they are stored
      if ind[-1]>ind[-2]:
        return vec2,vec1
      else:
        return vec1,vec2
      
  def getANumber(self):
    return 1
  
  def getEchoTrainLength(self):
      if self.mrProt.baseline().find('VB17')==-1 and 'EchoTrainLength' in self.dcm[0] and self.dcm[0].EchoTrainLength is not None:
          return int(self.dcm[0].EchoTrainLength)
      else:
          return self.mrProt.contrasts()
      
  def getTEs(self):
    if self.getEchoTrainLength()>1: 
      echo =[float(d.EchoTime) for d in self.dcm]
      echo, echo_i = np.unique(echo,return_inverse=True)
      return echo
    else:
      return self.dcm[0].EchoTime
    
  def getCoils(self):
    if self.type != 'SPECTRA':
      if [0x0051, 0x100F] in self.dcm[0]: 
        coil = [d[0x0051, 0x100F].value for d in self.dcm ]# coil ID in pvt header
        coil, coil_i = np.unique(coil,return_inverse=True)
        return coil
    elif 'ImageComments' in self.dcm[0]: 
        coil = [d['ImageComments'].value for d in self.dcm] # coil ID in pvt header
        coil, coil_i = np.unique(coil,return_inverse=True)
        return coil
      
    return 'Unknown'

  """
  Calculate Nifit headers
  return qform and json
  multi_TE: header provides average of all TE's
  """
  def getNiftiHeader(self,print_headers=False,i_ori=0):
    import datetime, json
    
    #spectroscopy related headers
    spectrometer_frequency_hz = self.mrProt.frequency()
    nucleus_str = '1H' #self.mrProt.nucleus()
    echo_time_s = np.mean(self.getTEs())*1e-3
    repetition_time_s = self.TR*1e-3
#    dwelltime = p[('sRXSPEC', 'alDwellTime', '0')]*self.Nppr*1e-9/self.Nti # pixdim[4]
    
    #scanner
    DeviceSerialNumber = self.dcm[0][0x0018,0x1000].value
    Manufacturer = self.dcm[0][0x0008,0x0070].value
    ManufacturersModelName = self.dcm[0][0x0008,0x1090].value
    SoftwareVersions = self.dcm[0][0x0018,0x1020].value

    #patient    
    PatientDoB = str(self.dcm[0][0x0010, 0x0030].value)
    PatientName = str(self.dcm[0][0x0010, 0x0010].value)
    PatientPosition = str(self.dcm[0][0x0018, 0x5100].value)
    PatientSex = str(self.dcm[0][0x0010, 0x0040].value     )
    PatientWeight = float(self.dcm[0][0x0010, 0x1030].value)

    
    conversion_method = 'Manual'
    conversion_time = datetime.datetime.now().isoformat(sep='T',timespec='milliseconds')
    original_file = [str(self.seriesDescription)]
    
      
    dim_dict = {}
    meta_dict = {**dim_dict,
            'SpectrometerFrequency':[spectrometer_frequency_hz,],
            'ResonantNucleus':[nucleus_str,],
            'EchoTime':echo_time_s,
            'RepetitionTime':repetition_time_s,             
            'DeviceSerialNumber':DeviceSerialNumber,
            'Manufacturer':Manufacturer,
            'ManufacturersModelName':ManufacturersModelName,
            'SoftwareVersions':SoftwareVersions,
            'PatientDoB':PatientDoB,
            'PatientName':PatientName,
            'PatientPosition':PatientPosition,
            'PatientSex':PatientSex,
            'PatientWeight':PatientWeight,
            'ConversionMethod':conversion_method,
            'ConversionTime':conversion_time,
            'OriginalFile':original_file}

    json_full = json.dumps(meta_dict)
    if print_headers:
        print(json_full)
    
    # Calculate the Orientation
    # Nifti: +x = Right; +y = Anterior; +z = Superior. That is, moving in the positive x-direction (so that the x-coordinate increases) will be moving to the right, et
    # Dicom: +x = Left;  +y = Posterior;+z = Superior  the x-axis is increasing to the left hand side of the patient. The y-axis is increasing to the posterior side of the patient. The z-axis is increasing toward the head of the patient.
    # qform = self.getOrientation()
    if 'SliceLocation' in self.ori[i_ori][0]:
      slc = [float(d.SliceLocation) for d in self.ori[i_ori]]
      slc, slc_i = np.unique(slc,return_inverse=True)
      NSLC = slc.size
      d_0 = self.ori[i_ori][slc_i.tolist().index(0)]
      d_end = self.ori[i_ori][slc_i.tolist().index(NSLC-1)]
    else:
      d_0 = self.ori[i_ori][0]
      NSLC = 1
    
    dir_cos = np.roll(np.array(d_0[0x0020,0x0037].value).reshape(2,3).transpose(),1,axis=1)
    # for pos use dicom of first slice (most )
    pos = np.array(d_0[0x0020,0x0032].value)
    
    # see https://nipy.org/nibabel/dicom/dicom_mosaic.html
    if self.isMosaic:
        NSLC = int(d_0[0x0019, 0x100A].value)
        nMosaic = np.ceil(np.sqrt(NSLC))
        md_row = int(d_0.Rows)
        md_col = int(d_0.Columns)
        rd_row =int(md_row/nMosaic)
        rd_col =int(md_col/nMosaic)
        ps = d_0.PixelSpacing
        pos = pos + np.matmul(dir_cos,np.array([(md_row-rd_row)*ps[0]/2,(md_col-rd_col)*ps[1]/2]))
        
        
        
    
    
    qform = np.zeros([4,4])
    #qform[:-1,1] = dir_cos[0,:]*d_0.PixelSpacing[0]
    #qform[:-1,0] = dir_cos[1,:]*d_0.PixelSpacing[1]
    qform[:-1,0] = dir_cos[:,0]*d_0.PixelSpacing[0]
    qform[:-1,1] = dir_cos[:,1]*d_0.PixelSpacing[1]
    

    if(NSLC>1) and not self.isMosaic: # multi slice or 3D
      qform[:-1,2] = (np.array(d_end[0x0020,0x0032].value) - pos)/(NSLC-1)
    else: # 2D
      dir_cos_z = np.cross(dir_cos[:,1],dir_cos[:,0])
      qform[:-1,2] = dir_cos_z*d_0.SliceThickness

      
    qform[0,3] = pos[0]
    qform[1,3] = pos[1]
    qform[2,3] = pos[2]
    qform[3,3] = 1
    
    # reverse X and Y axes
    qform[0,:] = -qform[0,:]
    qform[1,:] = -qform[1,:]

    return json_full, qform

  """
  data are saved in nii as they come in
  that is all echoes, repetitions and coils are saved to one volume 
  self.dims = ['row','col','slc','echo','coils','rep']
  """
  def saveNifti(self,fname,ima_in=None,i_ori=0):
    import nibabel as nib
    
    json_full,qform = self.getNiftiHeader(i_ori=i_ori)
    
    if type(ima_in) == type(None):
        ima_in = self.getImages(i_ori=i_ori)
        
    if ima_in.ndim>5: #shift repetitions from axis 5 to 3
        # assume order as self.dims
        ima_in = ima_in.transpose([0,1,2,5,3,4])
    
    #ima_in = ima_in.copy().transpose((0,1,2,3))
    newobj = nib.nifti2.Nifti2Image(ima_in, qform)
        # Set q_form >0
    #newobj.header.set_qform(qform)
    
    pixDim = newobj.header['pixdim']
    #pixDim[4] = dwelltime
    #pixDim[3] = self.img.
    newobj.header['pixdim'] = pixDim
    
    # Set version information 
    newobj.header['intent_name'] = b'dicompars'
    
    # Write extension with ecode 44
    extension = nib.nifti1.Nifti1Extension(44, json_full.encode('UTF-8'))
    newobj.header.extensions.append(extension)
    
    # # From nii obj and write    
    nib.save(newobj,fname)

  """ arrange according to getImages acq_i dim
  """
  def getBvals(self):
    bvals = np.zeros(max(self.acq_i)+1)
    for i in range(len(self.ori[0])):
      bvals[self.acq_i[i]] = int(self.ori[0][i][0x0019,0x100c].value) 
    
    return bvals

  def getBvecs(self):
    bvecs = np.zeros((max(self.acq_i)+1,3))
    for i in range(len(self.ori[0])):
      if [0x0019,0x100e] in self.ori[0][i]:
        bvecs[self.acq_i[i],:] = self.ori[0][i][0x0019,0x100e].value
    
    return bvecs
"""helper function to break down a mosaic image
"""
def deMosaic(ima_in,n_slc):
  mosaic = np.ceil(np.sqrt(n_slc))
  n_x = int(ima_in.shape[0]/mosaic)
  n_y = int(ima_in.shape[1]/mosaic)
  
  ima_out = np.ndarray([n_x,n_y,n_slc])
  
  for i in range(n_slc):
    mx = int(np.floor(i/mosaic))*n_x
    my = int(np.mod(i,mosaic))*n_y
    ima_out[:,:,i] = ima_in[mx:mx+n_x,my:my+n_y]
  
  return ima_out
