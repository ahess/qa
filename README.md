# qa

Quality assurance

generate a pandas table with QA parameters

dependancies: getopt, numpy, pandas

see QA_overview.docx https://git.fmrib.ox.ac.uk/ahess/qa/-/blob/master/QA_overview.docx 

usage:
python qa.py -i indir-o outfile -d

example output see https://users.fmrib.ox.ac.uk/~ahess/qa/index.html
