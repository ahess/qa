# -*- coding: utf-8 -*-
"""
Created on Tue Mar  2 14:25:28 2021

@author: aaron

Analyse QA scans in FMRIB and WIN

Useage:
  command line:
    python3 qa.py -p <inputdir> -o <outputfile> -w <outputhtml>
    -o write a pandas table using pd.to_pickel(outfile)
    -w write output to HTML and pandas table database
  Python:
    table = qa(fpath)
    path is directory with QA scan
    table is a pandas series for all QA parameters
    see getTable() for list and description of all types

Method:
  Data type detection based on series descriptions as follows:  
    Localiser:  'Localiser' or 'Localizer'
    EPI time series: 'ep2d_dailyqa'  or 'FMRIB Daily QA' 
    Noise spectra: 'fid_noise'
    RF Noise: 'rf_noise_spectrum'
    Uncombined GRE: 'gre' and '_UC'
    combined GRE: 'gre'
    MGH SNR: 'SNRMap'
    MGH noise: 'NoiseCovariance'
    
  Consistancy checking: 
    see getReference()
    
Uses:
  dicompars, epiqa, and AsciiMrProt
"""

import sys, argparse, numpy as np, pandas as pd, re, datetime, pickle
from pathlib import Path

import dicompars, epiqa, config, batch_analyse


dprint = False


def init_argparse() -> argparse.ArgumentParser:

    parser = argparse.ArgumentParser(
        description="Process DICOM files for scanner QA"
    )
    #usage="%(prog)s [OPTION] [FILE]...",
    parser.add_argument(

        "-i", "--version", action="version",

        version = f"{parser.prog} version 1.0.0"

    )

    parser.add_argument('--path','-p', required=True,type=str,nargs=1,help='path to dicom files')
    parser.add_argument('--out','-o', required=False,type=str,nargs=1,help='path where to save result as PANDAS file')
    parser.add_argument('--www','-w',  required=False,type=str,help='path to update HTML output and database')
    
    
    parser.add_argument('--verbose', '-v', action='count', default=0)
    return parser


# =============================================================================
# 
# =============================================================================
def main(argv):
      
   global dprint
   
   
   parser = init_argparse()
   args = parser.parse_args()
      
   
   
   if args.verbose:
       dprint = True
         

   if(dprint):  print ('Input file is ', args.path[0], '\nOutput file is ', args.out)
   
   table = qa(args.path[0])
   
   if args.out: 
       table.to_pickle(args.out[0])
   
   if args.www:
        if len(args.www) > 0:
           html_path = args.www
        else:
           html_path=config.html_path
          
        if(dprint):
            print('Saving to HTML at ' + html_path)
        file_id = saveToPickleDB(table,db_path=html_path)
        writeScannerAnalyseHTML(file_id,html_path=html_path)
        writeOverviewHTML(html_path=html_path)
     


# =============================================================================
# import dicom images
# generate QA parameters
# return parameters in pandas Series
# =============================================================================
def qa(inpath):
  global dprint
  p = dicompars.getPatients(inpath,True)
  if dprint and len(p) > 1: print('warning more than one patient found, using the fist')
   
  p = p[0]
  if dprint: print(p)

  
  table = getTable()
  
  d = p.series[0].dcm[0]
 
  table['Scanner Name'] = d.ManufacturerModelName
  table['Coil Name']    = p.series[0].mrProt.coilName()
  ad = d.AcquisitionDate
  at = d.AcquisitionTime
  table['DateTime']     = datetime.datetime(year=int(ad[0:4]),month=int(ad[4:6]),day=int(ad[6:]),hour=int(at[0:2]),minute=int(at[2:4]),second=int(at[4:6]))
  
  table['ScanID']       = d.PatientID
  table['TxReference']  = p.series[0].mrProt.referenceV()
  table['Frequency']    = float(d.ImagingFrequency)
  table['Scanner SerialNumber'] =  d.DeviceSerialNumber
  table['Scanner Software'] = p.series[0].mrProt.baseline()
  table['Institution'] = d.InstitutionName
  
  #Fill in optional entries
  # Localiser image
  sLoc = [s for s in p.series if 'localiser' in s.seriesDescription or 'localizer' in s.seriesDescription]
  if sLoc:
      localiserImg = np.concatenate(sLoc[0].getImagesAll(),axis=1)
      localiserImg = localiserImg[:,:,0].squeeze();
      table['Localiser'] = localiserImg
  
  # EPI QA
  sEPI = [s for s in p.series if 'ep2d_dailyqa' in s.seriesDescription or 'FMRIB Daily QA' in s.seriesDescription]
  for s in sEPI:
      my_epi = epiqa.EPIqa(s.getImages(0).squeeze(), s.TR/1000)      
      if my_epi.isValid:
        break
      
  if sEPI and my_epi.isValid:
      table['EPI tNSR Mean'], table['EPI tNSR'] = my_epi.tnsr_prc()
      #table['EPI SNR']
      table['EPI tStability']  = my_epi.timecourse_stability()
      table['EPI tTrend']      = my_epi.trend()
      table['EPI Ghost Level'] = my_epi.ghost_level()
  
  sNoise = [s for s in p.series if 'fid_noise' in s.seriesDescription]
  for s in sNoise:
    spec = s.getSpectra()
    if spec.shape[2]>1:  # it has a coils dimention
      # remove the first 256 samples, then join dim0 and 1
      spec = spec[128:-32,:,:] # discard start and end due to ringdown
      spec = np.reshape(spec,[np.prod(spec.shape[0:2]),spec.shape[2]])
      spec = np.moveaxis(spec,0,1)
      nCov = np.cov(spec)
      nCor = 10*np.log10(np.abs(np.corrcoef(spec)))
      table['Noise Correlation']= np.abs(nCor) # NxN 2D image
      table['Noise Covariance'] = np.abs(nCov) # NxN 2D image
      table['Noise Energy'] = np.abs(np.diag(nCov)).mean()
  
  sRF = [s for s in p.series if 'rf_noise_spectrum' in s.seriesDescription]
  if sRF:
      table['RF Noise'],table['RF Noise FLT'] = sRF[-1].getOOG_rf()
      table['RF Noise peak'] = np.max(table['RF Noise'][:,1])
      table['RF Noise FLT peak'] = np.max(table['RF Noise FLT'][:,1])
  
  sGRE = [s for s in p.series if 'gre' in s.seriesDescription and '_UC' in s.seriesDescription]
  
  if sGRE:
    use_gre = 0
    im_gre_uc2 = sGRE[use_gre].getImages()**2
    
    gre_e = im_gre_uc2.mean()
    ec = np.zeros([im_gre_uc2.shape[4],2])
    ec[:,0] = range(im_gre_uc2.shape[4])
    ec[:,1] = (im_gre_uc2.mean(axis=(0,1,2)).squeeze())/gre_e
    table['GRE coil energy'] = ec
    # use fid noise reference to determin SNR
    # this is simple per channel normalisation
    if 'dCov' in locals():
      nCov[0,0] = table['Noise Energy']   # bug in Siemens spec ice means Channel 1 is garbage
      sz = im_gre_uc2.shape
      gre_snr = np.divide(im_gre_uc2,np.abs(np.diag(nCov)).reshape(1,1,1,1,sz[4],1))
      table['GRE SNR'] = np.sqrt(gre_snr.sum())
  else:  # check for GRE coil combined
      sGRE = [s for s in p.series if 'gre' in s.seriesDescription]
      use_gre = -1
      
  if sGRE:
      im_gre = sGRE[use_gre+1].getImages()
      if im_gre.shape[4] == 1:
          table['GRE energy'] = (im_gre**2).sum()
      else:
          print('Unable to calculate GRE energy, wrong dims')
          
  # 7T Uses MGH tools for SNR and noise covariance
  # MGH tools SNR is calculated and stored in image comments
  sMGHSNR = [s for s in p.series if 'SNRMap' in s.seriesDescription]
  if sMGHSNR:
    im_cmt = [d['ImageComments'].value for d in sMGHSNR[0].dcm if 'rss SNR [' in d['ImageComments'].value]
    table['MGH RSS SNR'] = re.findall("[-+]?[.]?[\d]+(?:,\d\d\d)*[\.]?\d*(?:[eE][-+]?\d+)?",im_cmt[0])[0]
    
  # MGH tools NoiseCovariance
  # Overwrite FID based COV (above)
  sMGHCov = [s for s in p.series if 'NoiseCovariance' in s.seriesDescription]
  if sMGHCov:
    dCov = [d for d in sMGHCov[0].dcm if 'nCOVmtx mag [' in d['ImageComments'].value]
    dCof = [d for d in sMGHCov[0].dcm if 'nCOFmtx mag [' in d['ImageComments'].value]
    if dCov:
      nCov = dCov[0].pixel_array
      nCor = dCof[0].pixel_array
      sz = nCov.shape
      ds = int(sz[0]/4)
      
      ave_sc_off_n = [float(s) for s in re.findall("[-+]?[.]?[\d]+(?:,\d\d\d)*[\.]?\d*(?:[eE][-+]?\d+)?",dCov[0]['ImageComments'].value)]
      nCov = (nCov[ds:-ds:12,ds:-ds:12].copy())/ave_sc_off_n[1]
      
      ave_sc_off_n = [float(s) for s in re.findall("[-+]?[.]?[\d]+(?:,\d\d\d)*[\.]?\d*(?:[eE][-+]?\d+)?",dCof[0]['ImageComments'].value)]
      nCor = 10*np.log10(np.abs((nCor[ds:-ds:12,ds:-ds:12].copy())/ave_sc_off_n[1]))
      
      table['Noise Covariance'] = nCov
      table['Noise Correlation'] = nCor
      table['Noise Energy'] = np.abs(np.diag(nCov)).mean()
      
  #table['Out of range'] =         float('NaN'),
  #table['tSNR'],
  #table['Ghost Level':          float('NaN')})
  
  checkReference(table)
  addUnits(table)
  
  table['Comment'] = 'For testing only'
  table['Analsys Version'] = 0
  table['Protocol Version'] = 0
  return table

# =============================================================================
# Iniatalise pandas table for results
# Tabel row names are given here, empty
# Variable types:
#  1P:       a single numeric point - numpy array
#  1D:       a sequence of numbers (for plotting), has two cols, d[:,0]=x, d[:,1]=y
#  image 2D: an image -  2D numpy array
#  string:   text
# default to float('NaN') to aid python identification of missing data
# =============================================================================
def getTable():
  table = pd.Series({'Scanner Name':          float('NaN'), # string
                     'Coil Name':            float('NaN'), # string
                     'DateTime':             float('NaN'), # python datetime
#                     'Time':                 float('NaN'), # string
                     'Out of range':         [], # string
                     'Comment':              float('NaN'), # string
                     'ScanID':               float('NaN'), # string
                     'Localiser':            float('NaN'), # image 2D tiled by orientation 
                     'TxReference':          float('NaN'), # 1P
                     'Frequency':            float('NaN'), # 1P
                     'EPI tNSR':             float('NaN'), # image 2D tiled by slice
                     'EPI tNSR Mean':        float('NaN'), # 1P 
                     'EPI SNR':              float('NaN'),# 
                     'EPI tTrend':           float('NaN'),# 1P 
                     'EPI tStability':       float('NaN'),# 1P 
                     'EPI Ghost Level':      float('NaN'),# 1P 
                     'GRE energy':           float('NaN'),# 1P 
                     'GRE coil energy':      float('NaN'), # 1D coil energy (ratio of total energy)
                     'GRE SNR':              float('NaN'), # 1P  global SNR
                     'MGH RSS SNR':          float('NaN'), # 1P  global SNR
                     'Noise Energy':         float('NaN'), # 1P 
                     'Noise Correlation':    float('NaN'), # image 2D NxN
                     'Noise Covariance':     float('NaN'), # image 2D NxN
                     'RF Noise':             float('NaN'), # 1D  x-axis (frequency) and y-axis (energy a/u)
                     'RF Noise FLT':         float('NaN'), # 1D  x-axis (frequency) and y-axis (energy a/u)
                     'RF Noise peak':        float('NaN'), # 1P  peak from RF Noise1 - energy (a/u)
                     'RF Noise FLT peak':    float('NaN'), # 1P peak from RF Noise2 - energy (a/u)
                     'Scanner SerialNumber': float('NaN'), # string 
                     'Scanner Software':     float('NaN'), # string
                     'Analysis Version':     float('NaN'), # 1P 
                     'Protocol Version':     float('NaN'),
                     'Institution':          float('NaN'),
                     'Units':                [],
                     'Range':                []})
  
  return table

def addUnits(table):
  units = getTable()
  units['TxReference'] = 'V'
  units['Frequency'] = 'MHz'
  units['EPI tNSR'] = '%'
  units['EPI tNSR Mean'] = '%'
  units['EPI SNR'] = 'ratio'
  units['EPI tTrend'] = '% per vol'
  units['EPI tStability'] = '%'
  units['EPI Ghost Level'] = '%'
  units['GRE energy'] = 'au'
  units['GRE coil energy'] = 'au'
  units['GRE SNR'] = 'ratio'
  units['MGH RSS SNR'] = 'ratio'
  units['Noise Energy'] = 'au'
  units['Noise Correlation'] = 'dB'
  units['Noise Covariance'] = 'au'
  units['RF Noise'] = 'au'
  units['RF Noise FLT'] = 'au'
  units['RF Noise peak'] = 'au'
  units['RF Noise FLT peak'] = 'au'
  table['Units'] = units

"""
Check if parameters are within a reference range
append to 'Out of range' list in table
record reference table
"""
def checkReference(table):
  ref = config.getReference(table['Scanner SerialNumber'], table['Coil Name'])
  ax = ref.axes[0]
  table['Out of range'] = []
  for a in ax:
    if isinstance(ref[a],list):
      if isinstance(table[a],np.float64):
        if(table[a]<ref[a][0]) or (table[a]>ref[a][1]):
          table['Out of range'].append(a)
      elif isinstance(table[a],np.ndarray):
        dat = table[a]
        np.fill_diagonal(dat, np.nan) # ignonre diagonal
        dat = np.ma.array(dat, mask=np.isnan(dat))
        if (np.min(dat)<ref[a][0]) or (np.max(dat)>ref[a][1]):
          table['Out of range'].append(a)
    elif isinstance(ref[a],str):
      if ref[a] != table[a]:
        table['Out of range'].append(a)
        
  table['Range'] = ref
  
"""
Append the table object to a file named ScannerSN_CoilName.p
"""
def saveToPickleDB(table,db_path=config.data_path):
  # Save table to pickle list of data poitns
  file_id = table['Scanner SerialNumber']+'_'+table['Coil Name']
  fname_tables = db_path + '/QA_TABLES_' + file_id+ '.p'
  if(Path(fname_tables).is_file()):
    file = open(fname_tables, 'ab')
  else:
    dest = Path(db_path)
    if dest.is_dir():
      file = open(fname_tables, 'wb')
    else:
      print('Creating directory ' + db_path)
      dest.mkdir()
      file = open(fname_tables, 'wb')

  pickle.dump(table,file)
  file.close()
  
  # Record this as the last table for this scanner
  table.to_pickle( db_path+ '/QA_LAST_TABLE_' +file_id + '.p')
  
  return file_id

def loadFromPickleDB(file_id,db_path=config.data_path):
  fname_tables = db_path + '/QA_TABLES_' + file_id+ '.p'
  t_list = []
  with open(fname_tables, "rb") as f:
      while True:
          try:
              t_list.append( pickle.load(f))
          except EOFError:
              break
  return t_list

 
"""
  Write an analysis of QA parameters for coil 
  file_id is concatenation of scanner SN and coil name
"""  
def writeScannerAnalyseHTML(file_id,html_path=config.data_path):
   # Generate HTML using it
  fname_html = html_path + '/' + file_id+ '.html'
  
  if not Path(html_path).is_dir():
    print('Creating directory ' + html_path)
    Path(html_path).mkdir()
      
  batch_analyse.plotQA(loadFromPickleDB(file_id,db_path=html_path),to_html=fname_html,show_figures=False)
  
  
"""
  Write an overview talbe for all scanners as index.html in the html directory
  
"""
def writeOverviewHTML(html_path=config.data_path):
  pth = Path(html_path)
  f_list = [f for f in pth.iterdir() if f.is_file() and 'QA_LAST_TABLE_' in f.name]
  
  t_list = [pd.read_pickle(f) for f in f_list]
  
  # Want scanner name, coil name, date of last QA, and link to page
  scanners = [t['Scanner Name'] for t in t_list]
  institute = [t['Institution'] for t in t_list]
  scannerSN = [t['Scanner SerialNumber'] for t in t_list]
  coils = [t['Coil Name'] for t in t_list]
  # future set the colour based on if parameter is nan or not
  out_of_range = []
  for t in t_list:
    if isinstance(t['Out of range'],float) or not (t['Out of range']):
      out_of_range.append( '<p style="color:green"><b>All OK</b></p>' )
    else:
      out_of_range.append( '<p style="color:red"><b>'+str(t['Out of range'])+'</b></p>' )
                          #date = [t['DateTime'] for t in t_list]
  #<a href="url">link text</a>
  links = ['<a href="' + t['Scanner SerialNumber']+'_'+t['Coil Name'] + '.html'+'">'+str(t['DateTime']) + '</a>' for t in t_list]
  dwnHist = ['<a href="'+f.name +'">'+'All data' for f in pth.iterdir() if f.is_file() and 'QA_TABLES_' in f.name]
  dwnLast = ['<a href="'+f.name +'">'+'last scan' for f in pth.iterdir() if f.is_file() and 'QA_LAST_TABLE_' in f.name]
  
  df = pd.DataFrame({'Institution': institute,'Scanner':scanners,'Serial Number': scannerSN,'Coil':coils,'Last QA':links,'Out of range': out_of_range,'Download last QA':dwnLast,'Downlad all QA data':dwnHist})
  
  
  
  text_file = open(html_path+'/'+"index.html", "w")
  text_file.write(df.transpose().to_html(escape=False).replace("\\n","\n"))
  text_file.close()
  
if __name__ == "__main__":
   main(sys.argv[1:])
