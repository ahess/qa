# -*- coding: utf-8 -*-
"""
Created on Thu Mar  4 16:26:37 2021

@author: aaron
Extract and interprit Siemens Ascii protocl contained in ASCCONV section

"""

# =============================================================================
# Parser for Siemens ascii protocol
# =============================================================================
class AsciiMrProt(object):

    def __init__(self,ascii_prot):
        #scan text for ####ASCCONVTRANS##
        
        a = ascii_prot.split('### ASCCONV BEGIN object=')
        # VB17 does not use object=
        if len(a[0]) == len(ascii_prot):
          a = ascii_prot.split('### ASCCONV BEGIN ###')
        a = a[1].split('\n### ASCCONV END')
        a = a[0].replace('\t','')
        a = a.replace(' ','')
        lines = a.split('\n')
        
        
        self.data =[a.split('=') for a in lines]
        self.names = [d[0] for d in self.data]
        
        self.version = self.baseline()[3:5]  # VB VD VE XA
        
        
    def coilName(self):
        if self.version == 'VB':
          val = self.getVal('asCoilSelectMeas[0].asList[0].sCoilElementID.tCoilID')
        else:
          val = self.getVal('sCoilSelectMeas.aRxCoilSelectData[0].asList[0].sCoilElementID.tCoilID')
        return val.replace('"','')
    
    def protID(self):
        return self.getVal('lProtID')
    
    def frequency(self):
        return int(self.getVal('sTXSPEC.asNucleusInfo[0].lFrequency'))
    
    def referenceV(self):
        return float(self.getVal('sTXSPEC.asNucleusInfo[0].flReferenceAmplitude'))
    
    def baseline(self):
        val = self.getVal('sProtConsistencyInfo.tBaselineString')
        return val.replace('"','')
    
    def contrasts(self):
        return int(self.getVal('lContrasts'))
    
    def nucleus(self):
        return self.getVal('asCoilSelectMeas[0].tNucleus').strip('"')
        
    def getVal(self,name):
        ind =  self.names.index(name)
        return self.data[ind][1]