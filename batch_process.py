# -*- coding: utf-8 -*-
"""
Created on Wed May 19 16:29:28 2021

@author: ahess
"""

import qa, os,  pickle

dpath = 'D:/RawData/QA/'

scans = os.listdir(dpath)

#t3TP = [qa.qa(dpath+s) for s in scans if 'F3T_' in s]
table7T = []
for fDir in scans:
    if 'F7T_' in fDir:
      print(fDir)
      table7T.append(qa.qa(dpath+fDir))
      
              
save_path = 'G:/OneDrive - Nexus365/Work/WIN/QA/data'
file = open(save_path+'F7T_Tables.p', 'wb')  
pickle.dump(table7T, file)          
file.close()


"""
Read data back in
file = open(dpath+'F3T_Tables.p', 'rb')
intab = pickle.load(file)
file.close()
"""
